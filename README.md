## Build the image using Bitbucket workspace

docker-compose up -d

---

## Build the image using Docker hub images

1. Pull images of web-app & nginx server

docker pull tanquyvt/simpleservlet:multi-stage
docker pull tanquyvt/https-nginx:apply-volume

2. Create bridge network

docker network create --driver bridge <net-work_name>

3. Define local folder which contain cert files & nginx.conf. E.g, using workspace

4. Connect to user-defined network (run web-app first)

docker run -dit -p 8080:8080 --name web-app --network <network_name> tanquyvt/simpleservlet:multi-stage

docker run -dit -v ${PWD}/cert/:/etc/nginx/ssl/ -v ${PWD}/conf/nginx.conf:/etc/nginx/nginx.conf -p 5443:443 --name <server_name> --network <network_name> tanquyvt/https-nginx:apply-volume

Notes: ${PWD} have value of current directory (In this case, my Bitbucket workspace D:\MyDocker\nginx)

---

## Run the test

Access https://localhost:5443/app1
