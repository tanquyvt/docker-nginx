# Use an official runtime as a parent image
FROM nginx:latest

RUN apt-get update
RUN apt-get install openssl

RUN mkdir -p /etc/nginx/ssl
