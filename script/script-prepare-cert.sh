openssl genrsa -des3 -out server.key -passout pass:pasw0rd 2048

openssl req -new -passout pass:"pasw0rd" -subj "/C=JP/ST=Tokyo/L=Shibuya" -out server.csr

cp server.key server.key.org

openssl rsa -in server.key.org -passin pass:pasw0rd -out server.key

openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

cp server.crt /etc/nginx/

cp server.key /etc/nginx/